#!/usr/bin/perl

use strict;
use warnings;

open(PRENOMS, "prenoms") or die $!;
open(NOMS, "noms") or die $!;

my @prenoms = <PRENOMS>;
my @noms = <NOMS>;

close(NOMS);
close(PRENOMS);

my $prenom;
my $nom;

my %porte;
my %max = (1 => 1, 2 => 1, 3 => 1, 4 => 1);

while (my $ligne = <>)
{
  chomp ($ligne);
  
  if ($ligne =~ /^BEGIN:VCARD/)
  {
  	$prenom = $prenoms[rand (@prenoms)];
	$nom = $noms[rand (@noms)];
	chomp ($nom, $prenom);
   }
   elsif ($ligne =~ /^FN:/)
   {
   	 $ligne = "FN:$prenom $nom";
   }
   elsif ($ligne =~ /^N:/)
   {
   	 $ligne = "N:$nom;$prenom;;;";
   }
   elsif ($ligne =~ /^(TEL;TYPE=.+?):/)
   {
   	 my $tel = '0'.int(rand(999999999));
     $ligne = "$1:$tel";
   }
   elsif ($ligne =~ /^((DAVDROID1.)?EMAIL;.+?):/)
   {
     my $email = $prenom.int(rand(999)).'@'.$nom.'.ac';
     $ligne = "$1:$email";
   }
   elsif ($ligne =~ s/^CATEGORIES:([a-zA-Z0-9, -]+)/$1/)
   {
     my @categories = split (',', $ligne);
     my @all;
   	 foreach my $cat (@categories)
	 {
	 	if ($cat =~ /^[A-C]\d\d\d/)
		{
		  if (exists($porte{$cat}))
		  {
		     push (@all, $porte{$cat});
		  }
		  else
		  {
		    my $etage = (1,2,3,4)[int(rand (3))];
			  my $appart = $etage*100+$max{$etage}++;
        $porte{$cat} = $appart;
			  push (@all, $appart);
			  print "ORG:$appart\n";
		  }
		 }
		 else
		 {
		 	push(@all, $cat);
		 }
   }
   
   $ligne = 'CATEGORIES:'.join (',', @all);
  
  }
  
  print ("$ligne\n") unless($ligne =~ /^ORG:/);
   
}
