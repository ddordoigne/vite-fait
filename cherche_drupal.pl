#!/usr/bin/perl

use strict;
use warnings;
use LWP::UserAgent;

binmode(STDOUT, ":utf8");


# base de l'URL de recherche
my $drupal_url = 'https://www.april.org/';
my $drupal_query = 'type-de-publication/article-de-presse';


my $last_page = 99999;
my $current_page = 0;

my $ua = LWP::UserAgent->new();

# Pages de résultats de la recherche
while ($current_page < $last_page)
{
  my $resultat_recherche = $ua->get("$drupal_url/$drupal_query?page=$current_page");
  chomp $resultat_recherche;

  # Lignes de résultat de la recherche
  foreach my $ligne_qr (split /\n/, $resultat_recherche->decoded_content)
  {
    chomp  $ligne_qr;

    if ($ligne_qr =~ /^  <span class="views-field views-field-title">        <span class="field-content"><a href="(.+?)"/)
    {
      my $article = $ua->get("$drupal_url/$1");
      chomp $article;

      # Lignes de contenu de l'article
      my $quote = '';
      my $previous_content='';
      foreach my $ligne_article (split /\n/, $article->decoded_content)
      {
        chomp $ligne_article;
        
        # on supprime les liens (on les remplace par leur label en italique)
        $ligne_article =~ s/<a\s.+?>(.+?)<\/a>/<i>$1<\/i>/g;
        
        
        if ($ligne_article =~ /<br\s*\/>\s*$/i)
        {
          $previous_content.=$ligne_article;
        }
        else
        {
          $ligne_article = $previous_content.$ligne_article;
          $previous_content = '';
        }
        
        if ($ligne_article =~ /<blockquote><p><em>.*april.*<\/em><\/p><\/blockquote>/i)
        {
          $quote = $ligne_article;
        }
        elsif ($quote ne '' && $ligne_article =~ /article original[\s:]+<i>(.+?)<\/i>/)
        {
          print "<p><a href='$1'>$1</a>\n$quote</p><br />\n";   
          $quote = '';
        }
      }
    }
    elsif ($ligne_qr =~ /^<li class="pager-current last">(\d+)/)
    {
      $last_page = $1;
    }
  }
  $current_page++;
} 