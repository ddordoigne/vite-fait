#!/usr/bin/perl

use strict;
use warnings;

### initialisation

# capital à rembourser
my $capital = 33986.74;

# mensualite fixe
my $mensualite = 100;

# évolution de la mensualité selon la date
my %nouvelle_mens = ('2024-03' => -170, '2025-06' => 100); 

# taux initial
my $taux = 0.02;

# évolution du taux selon la date
my %nouveau_taux = (); 

# Message de fin
my $message = '';

# date de début
my $mois = 2;
my $annee = 2024;

#### calcul pour chaque année
print "mois;capital restant;mensualite;dont capital;dont intérets\n";

while ($capital)
{
  # cumuls pour l'année en cours
  my $y_capital  = 0; # capital remboursé
  my $y_interets = 0; # intérêts payés

  ### calcul pour chaque mois
  while ($mois <= 12)
  {
    # mois en cours de traitement (YYYY-MM)
    my $position = sprintf('%s-%02d', $annee, $mois);
    
    # application du nouveau taux en cas de changement
    if (defined($nouveau_taux{$position}))
    {
      $taux = $nouveau_taux{$position};
    }
    
    # application de la nouvelle mensualité taux en cas de changement
    if (defined($nouvelle_mens{$position}))
    {
      $mensualite = $nouvelle_mens{$position};
    }
    
    # interets a payer ce mois-ci
    my $interets = $capital*$taux/12;
  
    # on ne rembourse que le reliquat en dernière mensualité
    if ($capital <= $mensualite)
    {
      $mensualite = $capital + $interets;
    }
    
    my $remboursement = $mensualite - $interets;

    
    # affichage de l'etat en cours
    printf ("%s;%.2f;%.2f;%.2f;%.2f\n", $position, $capital, $mensualite, $remboursement, $interets);  

    $y_capital += $remboursement;
    $y_interets += $interets;
    $capital -= $remboursement;
    
    $mois++;
    last if ($capital <= 0);
  }
    $message .= sprintf ("total $annee :\t%.2f\t%.2f\t%.2f\n", $capital+$y_capital, $y_capital, $y_interets);

    $annee++;
    $mois=1;
}

# affichage du total annuel
print $message;
